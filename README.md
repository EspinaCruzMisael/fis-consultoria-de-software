# LA CONSULTORIA
```plantuml
@startmindmap
+[#lime]  LA CONSULTORIA 
++_ es
+++[#red] un servicio de profesionales de alto valor añadido
++++_ tiene
+++++[#white] nacimiento 
++++++_ que es 
+++++++[#silver] por las grandes organizaciones 
+++++++[#silver] gran complejidad interna 
+++++++[#silver] porcesos de sistemas de informacion compleja
+++++[#white] caracteristicas esenciales 
++++++_ como 
+++++++[#silver] conocimineto sectorial 
+++++++[#silver] conocimiento tecnico 
+++++++[#silver] capacidad de plantear soluciones 
+++++[#white] tipos de servicios que se prestan
++++++_ como
+++++++[#pink] consultoria
++++++++_ tiene
+++++++++[#lightgray] caracteristicas
++++++++++_ como 
+++++++++++[#violet] definicion de arquitectura en la base de datos 
+++++++++++[#violet] desarrollo de modelos analiticos avanzados
+++++++++++[#violet] procedimientos de operacion en la nube 
+++++++++[#lightgray] contiene
++++++++++[#violet] reingenieria de procesos 
+++++++++++[#violet] ayudar a la empresa a organizar sus actividades
++++++++++[#violet] gobierno TI
+++++++++++[#violet] son los metodos de desarrollo, la calidad\n y el poder atender los diferentes cambios
++++++++++[#violet] oficina de proyectos 
+++++++++++[#violet] es para la buena gestion de los proyectos 
++++++++++[#violet] analitica avanzada de datos 
+++++++++++[#violet] se encarga de saber que hacer con la informacion en tiempo real  
+++++++[#pink] integracion 
++++++++_ tiene 
+++++++++[#lightgray] caracteristicas 
++++++++++_ como
+++++++++++[#violet] desarrollo de cdm
+++++++++++[#violet] implementacion de estrategias en la nube 
+++++++++++[#violet] desarrollo de aplicaciones en la nube 
+++++++++++[#violet] integracion de entornos relacionados a la bigdata 
+++++++++[#lightgray] contiene 
++++++++++[#violet] desarrollo a medida
+++++++++++[#violet] se encarga de aplicar las tecnologias adecuadas 
++++++++++[#violet] aseguramiento de la calidad e2e
+++++++++++[#violet] participa en la solucion de los sistemas
++++++++++[#violet] infraestructuras
+++++++++++[#violet] practica que uno tienen con las plataformas  
++++++++++[#violet] soluciones de mercado 
+++++++++++[#violet] entiende el requerimiento que demanda el problema\n del cliente para darle una solucion viable 
+++++++[#pink] externalizacion
++++++++_ tiene
+++++++++[#lightgray] caracteristicas 
++++++++++_ como 
+++++++++++[#violet] mantenimiento de modelos analiticos 
+++++++++++[#violet] operaciones de servicio en la nube 
+++++++++[#lightgray] contiene 
++++++++++[#violet] gestion de aplicaciones 
+++++++++++[#violet] encomienda el matenimiento de los sistemas a un tercero 
++++++++++[#violet] servicios sqa
+++++++++++[#violet] su finalidad es evaluar la calidad de los procesos 
++++++++++[#violet] Operacion y ADM. de infraestructura
++++++++++[#violet] proceso de negocios 
+++++[#white] como es una profesion a futuro 
++++++[#lightgreen] es un modo de vida que te puede permitir alcanzar la felicidad profesional 
+++++++[#yellow] ofrece 
++++++++[#orange] un nivel importante de carrera profesional 
++++++++[#orange] retos en base a tu carrera profesional
+++++++[#yellow] tienen un papel clave 
++++++++_ en 
+++++++++[#orange] transformacion digital 
+++++++++[#orange] industria 4.0
+++++++++[#orange] smart cities
+++++[#white] exigencia de la consultoria 
++++++_ como 
+++++++[#azure] capacidad de trabajar en equipo 
+++++++[#azure] capacidad de evolocion 
+++++++[#azure] experiencia en las tics
+++++++[#azure] buena formacion y actitud 
+++++++[#azure] proactividad
++++++++[#azure] impresindible en la consultoria 
++++++++[#azure] implica iniciativa y anticipacion 
+++++++[#azure] voluntad de mejora 
++++++++[#azure] ideas de mejora 
++++++++[#azure] actitud en la construccion de empresa
+++++++[#azure] responsabilidad 
++++++++[#azure] asumir trabajos para hacerlo 
++++++++[#azure] no devolver nuevos problemas 
+++++++[#azure] disponibilidad total 
++++++++[#azure] trabajar mas horas 
+++++++[#azure] nuevos retos 
++++++++[#azure] actuar encima de nuestros conocimientos 
++++++++[#azure] permiten evolucionar mas rapido 
+++++++[#azure] viajar 
++++++++[#azure] suele demandar movilidad geagrafica
+++++[#white] como es la carrera profesional en consultoria
++++++_ tiene
+++++++[#lightblue] caracteristicas
++++++++_ como
+++++++++[#tan] flexible 
+++++++++[#tan] ajustado a las necesidades del mercado 
+++++++++[#tan] validas para diferentes tipos de servicio 
+++++++[#silver] niveles 
++++++++_ son
+++++++++[#gold] junior  
++++++++++[#gold] asistente 
+++++++++[#gold] senior
++++++++++[#gold] capacidad de desarrollar actividades por ti mismo  
+++++++++[#gold] gerente
++++++++++[#gold] capaccidad de definir solociones complejas 
+++++++++[#gold] director 
++++++++++[#gold] tiene responsabilidades completas 
@endmindmap
```

# Consultoria de Software
```plantuml
@startmindmap
+[#lightgreen] Consultoría de Software
++[#lime] actividades que se llevan a cabo 
+++_ como 
++++[#violet] desarrollo de software 
+++++_ como se hace 
++++++[#turquoise] comineza cuando el cliente tiene una necesidad en su negocio 
++++++[#turquoise] para ser mucho mas hagil 
++++++[#turquoise] mucho mas productivo
++++[#violet] ejemplo empresa  AXPE
+++++_ caracteristicas 
++++++[#turquoise]  dar servicio a empresas 
++++++[#turquoise] tener una muy buena infrestructura 
++++++[#turquoise] conocer a cada uno de tus clientes 
++++++[#turquoise] desarrolla 
+++++++[#orange] software de consultoria 
+++++++[#orange] software avanzado 
+++++++[#orange] presta servicio directamente a 4 paises 
+++++++[#orange] presta servicio indirectamente a 20 paises
++++[#violet] consultoria 
+++++[#turquoise] hablar con el cliente ver que requisitos va a necesitar 
+++++[#turquoise] que requerimientos se quiere crear
++++[#violet] estudio de viabilidad  
+++++_ hace
++++++[#orange] si es viable se hace 
+++++++[#orange] el diseño funcional 
++++++++[#orange] que informacion necestia para introducir en el sistema 
++++++++[#orange] que informacion te tiene que dar el sistema 
++++++++[#orange] esto se almacena en una base de datos
++++++[#orange] modelo de datos 
+++++++[#orange] cuantos procesos va a tener el sistema 
++++++[#orange] se hace un prototipo  
+++++++[#orange] para el cliente 
++++[#violet] diseño tecnico 
+++++_ caracteristicas 
++++++[#turquoise] aislamos el tipo de servio o de cliente de la parte tecnica 
++++++[#turquoise] aqui ya se traslada a los lenguajes de programacion
++++++[#turquoise] que tipo de base de datos se va a utilizar 
++++++[#turquoise] ya no se separa tanto la informacion 
++++++[#turquoise] depende mucho del tipo de metodologia que se utiliza 
++++[#violet] prueba integrada
+++++[#white] prueba de que todos los componentes funcionen
++++[#violet] prueba funcional 
+++++[#azure] es caundo ya se prueba las especificaiones del diseño tecnico y diseño funcional
+++++[#azure] si esta bien se le pasa al cliente 
++++++[#azure] se hacen las pruebas de usuario si el cliente dio el ok 
++++[#violet] recursos que se utiliza 
+++++[#lime] jefe de proyecto
++++++_ es 
+++++++[#pink] el encargado de planificar la fases del proyecto
++++++++_ caracteristicas 
+++++++[#pink]  que recursos se necesitan  para el proyecto 
+++++++[#pink]  de hablar con el cliente 
+++++++[#pink]  coordinar todas las tareas  
+++++++[#pink]  dotar al proyecto de los recursos 
+++++++[#pink]  influye mucho el prueba funcional 
++++[#violet] como se materializa los procesos 
+++++[#silver] es depende de como quiere el cliente 
++++++[#silver] ya que se puede desarrollar en sus instalaciones 
++++++[#silver] o en las instalaciones 
++++++[#silver] tratar de industrializar el software 
+++++++[#silver] con la factoria de software 
++++[#violet] tendencia actuales 
+++++_ son 
++++++[#azure] cambian como es la infrestructura de la empresa 
++++++[#azure] empezar a utilar la nube 
+++++++[#azure] la seguridad ya no la realizas tu 
+++++++[#azure] tienes tareas externalizadas 
+++++++[#azure] la informacion de antes se empieza a utilizar 
++++++[#azure] automatizacion 
++++++[#azure] se hace una metodologia mas agil 
++++++[#azure] ya se separan el proceso de las pruebas 
++++++[#lightblue] desventajas 
+++++++_ tales como
++++++++[#lightblue] dado que las empresas dueñas de las nuves estan en otros paises 
+++++++++[#lightblue] las leyes pueden llegar a ser un problema 
++++++++[#lightblue] es diferente las leyes en la proteccion 
++++++++[#lightblue] no depende de ti la seguridad 
+++++++++[#lightblue] tu informacion esta disponible para el dueño de la nube 
@endmindmap
```
# Aplicacion de la Ingenieria de Software 
```plantuml
@startmindmap
+[#red] Aplicacion de la Ingeniería de Software
++_ tienen
+++[#turquoise] solocuiones rp
++++_ es
+++++[#violet] cubrir todos los procesos de negocio que hay dentro de una empresa
+++++[#violet] es un paquete software modificable 
+++++[#violet] definir el proceso de negocio punto a punto 
+++[#turquoise] procedimiento para la aplicacion
++++_ debe tener 
+++++[#lime] tener un enfoque metodologico 
++++++[#silver] tener un vision global 
++++++[#silver] entender todos los procesos
++++++[#silver] reunirse con todad las areas 
++++++[#silver] trazabilidad 
+++++[#lime] diseño fucional 
+++++[#lime] gestion documental
+++++[#lime] los cambios si se documentan y deben ser aprobados por los clientes
+++++[#lime] anlisis tecnico 
++++++[#silver] como hacer las cosas 
+++++++[#silver] en la parametrizacion 
+++++++[#silver] en la programacion
+++[#turquoise] fase 
++++_ tales como
+++++[#yellow] analisis 
++++++[#orange] lograr saber y entender los procesos 
+++++[#yellow] construccion 
++++++_ tiene 
+++++++[#orange] prototipado 
+++++++[#orange] desarrollar los requerimientos solicitados
+++++[#yellow] pruebas y formacion 
++++++[#orange] correccion de errores 
++++++[#orange] pruenas para la deteccion de errores
+++++[#yellow] gestion de alcance 
++++++[#orange] identificar que cambio a habido 
++++++[#orange] rentabilidad del proyecto
+++[#turquoise] herramientas 
++++_ como 
+++++[#lime] heramientas de procesos 
++++++[#lightgreen] aris 
++++++[#lightgreen] identificar y docmumetar los procesos
++++++[#lightgreen] hacer los levantamientos que tiene el proceso  
++++++[#lightgreen] ficheros en un editor de texto 
++++++[#lightgreen] no es tan importante la herramienta de la gestion documental 
+++[#turquoise] se divide 
++++_ en 
+++++[#azure] consultores funcionales 
++++++[#white] se dedican a configurar la herramienta 
+++++[#azure] programacion 
++++++[#white] cada requisito funcional debe tener diseños tecnicos 
++++++[#white] la documentacion es vital 
++++++[#white] es importanate un diseño tecnico para poder empezar a programar 
@endmindmap
```
